package radioserver

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestSetVolumeWpctl(t *testing.T) {
	os.Setenv("AUDIO_OUTPUT_DEVICE", "@DEFAULT_AUDIO_SINK@")
	// get
	volume, err := GetVolumeWpctl()
	if err != nil {
		t.Error(err)
	}
	origVolume := volume
	println(volume)

	// set
	new := 1
	if err := SetVolumeWpctl(new); err != nil {
		t.Error(err)
	}
	time.Sleep(time.Second)

	volume, err = GetVolumeWpctl()
	if err != nil {
		t.Error(err)
	}

	println(volume)

	assert.Equal(t, volume, new, "Expected and real volume are different")

	// revert
	if err := SetVolumeWpctl(origVolume); err != nil {
		t.Error(err)
	}
	time.Sleep(time.Second)

	volume, err = GetVolumeWpctl()
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, volume, origVolume, "Expected and real volume are different")
}
