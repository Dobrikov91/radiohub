package radioserver

import (
	"fmt"
	"net/http"
	"os"
	"os/exec"
)

type Player struct {
	running         bool
	command         *exec.Cmd
	app             string
	playArgsPre     []string
	playArgsPost    []string
	playUrlArgsPre  []string
	playUrlArgsPost []string
}

func newVlcPlayer() *Player {
	player := new(Player)
	player.app = "vlc"
	player.playArgsPre = []string{"-Idummy"}
	player.playArgsPost = []string{"vlc://quit"}
	player.playUrlArgsPre = []string{"-Idummy"}

	return player
}

// func newFfmpegPlayer() *Player {
// 	player := new(Player)
// 	player.app = "ffplay"
// 	player.playArgsPre = []string{"-nodisp", "-autoexit"}
// 	player.playUrlArgsPre = []string{"-nodisp"}

// 	return player
// }

// Play creates a task to play file till the end
func (Player *Player) Play(fileName string) error {
	// don't store command since we don't need to track it
	command := exec.Command(Player.app,
		append(append(Player.playArgsPre, fileName), Player.playArgsPost...)...)
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	return command.Start()
}

// PlayUrl creates a task to playing audio by url link endlessly
func (Player *Player) PlayUrl(url string) error {
	if Player.running {
		if err := Player.Stop(); err != nil {
			return err
		}
	}
	Player.command = exec.Command(Player.app,
		append(append(Player.playUrlArgsPre, url), Player.playUrlArgsPost...)...)
	Player.command.Stdout = os.Stdout
	Player.command.Stderr = os.Stderr
	Player.running = true

	resp, err := http.Get(url)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return fmt.Errorf("BAD RESPONSE CODE: %d", resp.StatusCode)
	}
	return Player.command.Start()
}

// Stop stops current PlayUrl task
func (Player *Player) Stop() error {
	if !Player.running {
		return nil
	}
	if Player.command.Process == nil {
		return nil
	}

	if err := Player.command.Process.Kill(); err != nil {
		return err
	}
	Player.running = false
	return nil
}
