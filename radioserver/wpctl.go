package radioserver

import (
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strconv"
)

func GetVolumeWpctl() (int, error) {
	device := os.Getenv("AUDIO_OUTPUT_DEVICE")
	out, err := exec.Command(
		"wpctl", "get-volume", device).Output()

	if err != nil {
		return 0, err
	}

	re := regexp.MustCompile(`Volume:\s*([+-]?\d+(\.\d+)?)`)
	match := re.FindStringSubmatch(string(out))

	if len(match) > 1 {
		volume, err := strconv.ParseFloat(match[1], 64)
		if err != nil {
			fmt.Printf("Error parsing float: %v\n", err)
			return 0, err
		}
		volumePercent := int(volume * 100)
		fmt.Printf("Parsed volume: %d\n", volumePercent)
		return volumePercent, nil
	}
	return 0, err
}

func SetVolumeWpctl(val int) error {
	device := os.Getenv("AUDIO_OUTPUT_DEVICE")

	cmd := exec.Command(
		"wpctl", "set-volume", device,
		strconv.Itoa(val)+"%")
	return cmd.Start()
}
