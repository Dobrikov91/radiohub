package radioserver

import (
	"testing"
	"time"
)

func TestRadio(t *testing.T) {
	radio := NewRadio()
	time.Sleep(2 * time.Second)

	// file
	if err := radio.Play("https://download.samplelib.com/mp3/sample-3s.mp3", "Sample mp3"); err != nil {
		t.Error(err)
	}
	time.Sleep(3 * time.Second)

	// bad link
	if err := radio.Play("https://nubreaksradio.radioca.st/listen.pls", "Sample mp3"); err == nil {
		t.Error("No error on bad url")
	}
	time.Sleep(2 * time.Second)

	radio.Stop()
}

// TODO: set volume tests
