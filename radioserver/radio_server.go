package radioserver

// https://go.dev/doc/tutorial/web-service-gin

import (
	"fmt"
	"net/http"
	"radiohub/database"
	"strconv"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var radio = NewRadio()

type PlayRequest struct {
	Url  string `json:"url"`
	Name string `json:"name"`
}

func Run(port string) {
	router := gin.Default()

	router.Use(cors.Default())
	router.POST("/play", play)
	router.GET("/playId/:value", playId)
	router.GET("/stop", stop)
	router.GET("/volume/:value", volume)
	router.GET("/volume/get", getVolume)

	router.Run(":" + port)
}

func play(c *gin.Context) {
	var req PlayRequest

	if err := c.BindJSON(&req); err != nil {
		c.IndentedJSON(http.StatusBadRequest, fmt.Sprintf("Parse error: %s", err.Error()))
		return
	}

	if err := radio.Play(req.Url, req.Name); err != nil {
		c.IndentedJSON(http.StatusInternalServerError, fmt.Sprintf("Play error: %s", err.Error()))
		return
	}

	c.IndentedJSON(http.StatusOK, nil)
}

func playId(c *gin.Context) {
	value, err := strconv.Atoi(c.Param("value"))
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, fmt.Sprintf("Parse error: %s", err.Error()))
		return
	}

	radioStationListPath := "./data/radioStations.json"
	radioStations, err := database.FileGetRadioStations(radioStationListPath)
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, fmt.Sprintf("Database issue: %s", err.Error()))
		return
	}

	if value < 0 || value >= len(radioStations) {
		c.IndentedJSON(http.StatusBadRequest, fmt.Sprintf("Radio station index %d out of range", value))
		return
	}

	url := radioStations[value].Url
	stationName := radioStations[value].Name

	if err := radio.Play(url, stationName); err != nil {
		c.IndentedJSON(http.StatusInternalServerError, fmt.Sprintf("Play error: %s", err.Error()))
		return
	}

	c.IndentedJSON(http.StatusOK, nil)
}

func stop(c *gin.Context) {
	if err := radio.Stop(); err != nil {
		c.IndentedJSON(http.StatusInternalServerError, fmt.Sprintf("Stop error: %s", err.Error()))
		return
	}

	c.IndentedJSON(http.StatusOK, nil)
}

func volume(c *gin.Context) {
	value, err := strconv.Atoi(c.Param("value"))
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, fmt.Sprintf("Parse error: %s", err.Error()))
		return
	}

	if err := radio.Volume(value); err != nil {
		c.IndentedJSON(http.StatusInternalServerError, fmt.Sprintf("Set volume error: %s", err.Error()))
		return
	}
	c.IndentedJSON(http.StatusOK, nil)
}

func getVolume(c *gin.Context) {
	value, err := radio.GetVolume()

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, fmt.Sprintf("Get volume error: %s", err.Error()))
		return
	}
	c.IndentedJSON(http.StatusOK, value)
}
