package radioserver

import (
	"os"
	"os/exec"
	"regexp"
	"strconv"
)

func GetVolumePactl() (int, error) {
	device := os.Getenv("AUDIO_OUTPUT_DEVICE")
	out, err := exec.Command(
		"pactl", "get-sink-volume", device).Output()

	if err != nil {
		return 0, err
	}

	r := regexp.MustCompile(`\d+(?:\.\d+)?%`)
	val := r.FindString(string(out))
	if val == "" {
		return 0, &CantReadVolumeError{}
	}

	ival, err := strconv.Atoi(val[:len(val)-1])
	if err != nil {
		return 0, err
	}

	return ival, nil
}

func SetVolumePactl(val int) error {
	device := os.Getenv("AUDIO_OUTPUT_DEVICE")

	cmd := exec.Command(
		"pactl", "set-sink-volume", device,
		strconv.Itoa(val)+"%")
	return cmd.Start()
}
