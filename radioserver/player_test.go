package radioserver

import (
	"fmt"
	"testing"
	"time"
)

func TestPlayer(t *testing.T) {
	player := newVlcPlayer()

	// initial stop
	fmt.Println("Initial stop")
	if err := player.Stop(); err != nil {
		t.Error(err)
	}

	// playFile
	if err := player.Play("https://download.samplelib.com/mp3/sample-3s.mp3"); err != nil {
		t.Error(err)
	}
	time.Sleep(2 * time.Second)

	if err := player.Stop(); err != nil {
		t.Error(err)
	}
}

func TestPlayerGoodUrl(t *testing.T) {
	player := newVlcPlayer()

	if err := player.PlayUrl("http://silverrain.hostingradio.ru/silver128.mp3"); err != nil {
		t.Error(err)
	}

	time.Sleep(3 * time.Second)
	if err := player.Stop(); err != nil {
		t.Error(err)
	}
}

func TestPlayerBadUrl(t *testing.T) {
	player := newVlcPlayer()

	if err := player.PlayUrl("https://nubreaksradio.radioca.st/listen.pls"); err == nil {
		t.Error("No error on bad url")
	}
}

func TestSeveralPlaybacks(t *testing.T) {
	player := newVlcPlayer()

	for i := 0; i < 3; i++ {
		if err := player.Play("https://download.samplelib.com/mp3/sample-3s.mp3"); err != nil {
			t.Error(err)
		}
		time.Sleep(1 * time.Second)
	}

	if err := player.Stop(); err != nil {
		t.Error(err)
	}
}
