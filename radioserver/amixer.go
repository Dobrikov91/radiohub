package radioserver

import (
	"os"
	"os/exec"
	"regexp"
	"strconv"
)

type CantReadVolumeError struct{}

func (m *CantReadVolumeError) Error() string {
	return "Cant read volume from amixer"
}

func GetVolume() (int, error) {
	device := os.Getenv("AUDIO_OUTPUT_DEVICE")
	control := os.Getenv("AMIXER_CONTROL")

	out, err := exec.Command(
		"amixer", "-c", device,
		"get", control).Output()

	if err != nil {
		return 0, err
	}

	r := regexp.MustCompile(`\d+(?:\.\d+)?%`)
	val := r.FindString(string(out))
	if val == "" {
		return 0, &CantReadVolumeError{}
	}

	ival, err := strconv.Atoi(val[:len(val)-1])
	if err != nil {
		return 0, err
	}

	return ival, nil
}

func SetVolume(val int) error {
	device := os.Getenv("AUDIO_OUTPUT_DEVICE")
	control := os.Getenv("AMIXER_CONTROL")

	cmd := exec.Command(
		"amixer", "-c", device,
		"set", control, strconv.Itoa(val)+"%")
	return cmd.Start()
}
