package radioserver

import (
	"fmt"
	"os"

	htgotts "github.com/hegedustibor/htgo-tts"
	voices "github.com/hegedustibor/htgo-tts/voices"
)

type Radio struct {
	announcer htgotts.Speech
	player    *Player
}

func NewRadio() *Radio {
	radio := new(Radio)
	radio.player = newVlcPlayer()

	radio.announcer = htgotts.Speech{Folder: "/tmp/radiohub/audio", Language: voices.English, Handler: radio.player}
	radio.announcer.Speak("RadioHub created")

	return radio
}

func (Radio *Radio) Play(url string, stationName string) error {
	if err := Radio.player.PlayUrl(url); err != nil {
		fmt.Println(err)

		if err := Radio.announcer.Speak("Can't play " + stationName); err != nil {
			fmt.Println(err)
			return err
		}

		return err
	}

	if err := Radio.announcer.Speak("Playing " + stationName); err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

func (Radio *Radio) Stop() error {
	Radio.announcer.Speak("Stop playing")
	if err := Radio.player.Stop(); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (Radio *Radio) Volume(value int) error {
	system := os.Getenv("AUDIO_SYSTEM")
	if system == "Pipewire" {
		return SetVolumeWpctl(value)
	}
	if system == "Pulseaudio" {
		return SetVolumePactl(value)
	}
	return SetVolume(value)
}

func (Radio *Radio) GetVolume() (int, error) {
	system := os.Getenv("AUDIO_SYSTEM")
	if system == "Pipewire" {
		return GetVolumeWpctl()
	}
	if system == "Pulseaudio" {
		return GetVolumePactl()
	}
	return GetVolume()
}
