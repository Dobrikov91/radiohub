package radioserver

import (
	"bytes"
	"encoding/json"
	"net/http"
	"testing"
	"time"
)

var hostUrl = "http://localhost:8085"

func sendRequest(url string) error {
	_, err := http.Get(url)
	return err
}

func sendRequestJson(url string, data []byte) error {
	request, err := http.NewRequest("POST", url, bytes.NewReader(data))
	if err != nil {
		return nil
	}
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return err
	}
	defer response.Body.Close()
	return nil
}

func TestServer(t *testing.T) {
	go Run("8085")

	// Initial stop
	time.Sleep(1 * time.Second)
	if err := sendRequest(hostUrl + "/stop"); err != nil {
		t.Errorf("Error sending request: %v\n", err)
	}

	// play radio
	time.Sleep(1 * time.Second)
	marshalled, err := json.Marshal(PlayRequest{Name: "Silver", Url: "http://silverrain.hostingradio.ru/silver128.mp3"})
	if err != nil {
		t.Errorf("Error making json: %v\n", err)
	}

	if err := sendRequestJson(hostUrl+"/play", marshalled); err != nil {
		t.Errorf("Error sending request: %v\n", err)
	}

	// Final stop
	time.Sleep(2 * time.Second)
	if err := sendRequest(hostUrl + "/stop"); err != nil {
		t.Errorf("Error sending request: %v\n", err)
	}
}

func TestServerStop(t *testing.T) {
	go Run("8085")

	// Initial stop
	time.Sleep(1 * time.Second)
	if err := sendRequest(hostUrl + "/stop"); err != nil {
		t.Errorf("Error sending request: %v\n", err)
	}
}

func TestServerVolume(t *testing.T) {
	go Run("8085")

	time.Sleep(1 * time.Second)
	if err := sendRequest(hostUrl + "/volume/get"); err != nil {
		t.Errorf("Error sending request: %v\n", err)
	}

	time.Sleep(1 * time.Second)
	if err := sendRequest(hostUrl + "/volume/1"); err != nil {
		t.Errorf("Error sending request: %v\n", err)
	}
}
