package radioserver

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetVolume(t *testing.T) {
	os.Setenv("AUDIO_OUTPUT_DEVICE", "Intel")
	os.Setenv("AMIXER_CONTROL", "PCM")
	// get
	volume, err := GetVolume()
	if err != nil {
		t.Error(err)
	}
	origVolume := volume
	println(volume)

	// set
	new := 1
	if err := SetVolume(new); err != nil {
		t.Error(err)
	}

	volume, err = GetVolume()
	if err != nil {
		t.Error(err)
	}

	println(volume)

	assert.Equal(t, volume, new, "Expected and real volume are different")

	// revert
	if err := SetVolume(origVolume); err != nil {
		t.Error(err)
	}

	volume, err = GetVolume()
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, volume, origVolume, "Expected and real volume are different")
}
