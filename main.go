package main

import (
	"fmt"
	"os"
	"radiohub/radioserver"
)

func main() {
	radioServerAddress := os.Getenv("HOSTNAME")
	if radioServerAddress == "" {
		fmt.Println("Set your 'HOSTNAME' environment variable. Set to 'localhost'")
		radioServerAddress = "localhost"
	}
	radioServerAddress = "http://" + radioServerAddress
	radioServerPort := "8085"

	go webserver("./webpage", "8080", radioServerAddress, radioServerPort)

	radioserver.Run(radioServerPort)
}
