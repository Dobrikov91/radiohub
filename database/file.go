package database

import (
	"encoding/json"
	"os"
)

func FileGetRadioStations(path string) (RadioStations, error) {
	fileBytes, _ := os.ReadFile(path)
	radioStations := RadioStations{}
	err := json.Unmarshal(fileBytes, &radioStations)
	if err != nil {
		return RadioStations{}, err
	}
	return radioStations, nil
}
