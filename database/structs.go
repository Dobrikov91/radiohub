package database

type RadioStations []struct {
	ID      string `json:"_id,omitempty"`
	Name    string `json:"name"`
	Url     string `json:"url"`
	Website string `json:"website,omitempty"`
	Logo    string `json:"logo,omitempty"`
}
