package database

import (
	"fmt"
	"testing"
)

func TestRadioStationsConfigParser(t *testing.T) {
	path := "../data/radioStations.json"
	stations, err := FileGetRadioStations(path)
	if err != nil {
		t.Error("Can't parse config", path, err)
	}

	fmt.Println(stations)

	if stations[0].Name != "Sub FM" {
		t.Error("Wrong station", stations[1].Name, "expected Sub FM")
	}

	if stations[1].Url != "https://threads.out.airtime.pro/threads_a" {
		t.Error("Wrong station", stations[1].Url, "expected Sub FM")
	}

	path = "./hehe.json"
	_, err = FileGetRadioStations(path)
	if err == nil {
		t.Error("Non-existent file parsed correctly")
	}
}
