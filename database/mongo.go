package database

import (
	"context"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func Connect() *mongo.Client {
	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		log.Fatal("Set your 'MONGODB_URI' environment variable. " +
			"See: www.mongodb.com/docs/drivers/go/current/usage-examples/#environment-variable")
	}
	client, err := mongo.Connect(context.TODO(), options.Client().
		ApplyURI(uri))
	if err != nil {
		panic(err)
	}

	return client
}

func MongoGetRadioStations() (RadioStations, error) {
	client := Connect()

	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	coll := client.Database("radiohub").Collection("radio-links")
	cursor, err := coll.Find(context.TODO(), bson.D{})
	if err != nil {
		return RadioStations{}, err
	}

	var results RadioStations
	if err = cursor.All(context.TODO(), &results); err != nil {
		return RadioStations{}, err
	}

	return results, nil
}
