# build stage
FROM golang:latest AS builder
RUN apt-get update && apt-get install -y \
    alsa-utils \
    libasound2 \
    libasound2-dev \
    libasound2-plugins

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download
COPY . .

RUN go build -o radiohub .

# final stage
FROM debian:latest
RUN apt-get update && apt-get install -y \
    vlc-bin vlc-plugin-base \
    pipewire pipewire-alsa alsa-utils

RUN useradd -m -s /bin/bash vlcuser
USER vlcuser

WORKDIR /app
COPY --from=builder /app/radiohub .
COPY ./webpage ./webpage

CMD ["./radiohub"]
