package main

import (
	"html/template"
	"net/http"
)

type RadioServerData struct {
	Address   string
	RadioPort string
	WebPort   string
}

func webserver(webPagePath string, webPort string, radioAddress string, radioPort string) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		data := RadioServerData{
			Address:   radioAddress,
			RadioPort: radioPort,
			WebPort:   webPort,
		}
		tmpl, _ := template.ParseFiles(webPagePath + "/index.html")
		tmpl.Execute(w, data)
	})
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir(webPagePath+"/css"))))
	http.Handle("/data/", http.StripPrefix("/data/", http.FileServer(http.Dir(webPagePath+"/../data"))))

	http.HandleFunc("/stations", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.ServeFile(w, r, webPagePath+"/../data/radioStations.json")
	})

	http.ListenAndServe(":"+webPort, nil)
}
