# Radiohub

## Description
Radiohub is a small pet project for myself as an internet radio enthusiast.
I always wanted to have radio  playing in the background, without interference from other audio sources. I also didn't like having a million browser tabs open or moving the player app config between systems.
I wanted to make a simple device that plays a radio by a single button press and I made it.

## Setup
You need:
- RaspberryPi (ideally) or any linux machine
- (optional) Good audiohat for Raspberry. I'm using Beocreate Hifiberry https://www.hifiberry.com/beocreate/ This device replaced a big home amplifier for me
- (optional) Midi controller. If you want to add physical buttons without touching GPIO

For Docker version you have to switch to pipewire audio system. The tool requires audio mixing, so pure alsa isn't an option (I dont want to modify system config). Pulseaudio isn't supported with Docker option now.

1. sudo raspi-config > Advanced options > Audio Config > Pipewire
2. Reboot RaspberryPi

## Configuration
0. The tool uses default pipewire playback device. Change pipewire output on the host system to configure output.

1. Set environment variables or modify `compose.yaml` file. Required for webserver and volume control.
```
  - HOSTNAME=your-device-host-name.local # required for webserver part
  # Volume control
  - AUDIO_SYSTEM=Pipewire # Pipewire | Alsa (default: Alsa)
  - AUDIO_OUTPUT_DEVICE=@DEFAULT_AUDIO_SINK@ # Wireplumber/Alsa
  # - AMIXER_CONTROL=PCM # Only for Alsa, find from alsamixer tool
```

2. Create/edit file `radioStations.json` with your favorite station list in json format. Setup path in `compose.yaml` if required. Example (fields `name` and `url` are mandatory)
```
[
  {
    "name": "Sub FM",
    "url": "https://subfm.radioca.st/stream",
    "website": "https://www.sub.fm",
    "logo": "https://www.sub.fm/wp-content/uploads/2021/07/white-sub-300x300.png"
  },
  {
    "name": "Threads",
    "url": "https://threads.out.airtime.pro/threads_a",
    "website": "https://threadsradio.com/en",
    "logo": "https://i1.sndcdn.com/avatars-vl4EXze4d1rN4cUr-nndcnQ-t500x500.jpg"
  }
]
```

## Run
### Native
1. Install golang
2. Install all dependencies from `compose.yml`
3. Run tool with `go run .`

### Docker
1. Install docker and docker-compose
2. Build docker image `sudo docker-compose build`
3. Run container `sudo docker-compose up -d`

optional:
for Beocreate Hifiberry run another docker image for an easy setup: https://hub.docker.com/repository/docker/dobrikov91/hifiberry-dsp/

## Usage

** Web UI **
Go to `http://hostname.local:8080` to start playback or control volume.

** Midi controller **
At the moment I support only nanoKONTROL2 and Launchkey Mini MK3. Connect device, restart container. Move first slider to setup volume, press buttons to play 1-8 radio link from the list.

## Project status
very alfa
- tool has limited configuration option. List of radio links, params for midi server and playback device are hardcoded. Maybe I'll fix it in the future
- docker image in the official docker repo should be ready soon

## License
I don't care about license at the moment. Should me MIT